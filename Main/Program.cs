﻿using System;
using System.CommandLine;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using JRevolt.Configuration.CommandLine;
using Serilog;
using Serilog.Debugging;
using Serilog.Events;

[assembly:InternalsVisibleTo("JRevolt.YamlSecrets.Tool")]
namespace JRevolt.YamlSecrets.Cli
{
  internal class Program
  {
    internal static async Task<int> Main(string[] args)
    {
      // setup logging
      var sLogLevel = Environment.GetEnvironmentVariable("SERILOG");
      if (!Enum.TryParse<LogEventLevel>(sLogLevel, out var logLevel)) logLevel = LogEventLevel.Warning;
      if (logLevel <= LogEventLevel.Debug) SelfLog.Enable(Console.WriteLine);
      var template = "[{Timestamp:HH:mm:ss}|{Level:u3}|{SourceContext}] {Message:lj} {NewLine}{Exception}";
      Log.Logger = new LoggerConfiguration()
        .WriteTo.Console(logLevel, template, standardErrorFromLevel: LogEventLevel.Verbose)
        .MinimumLevel.Is(logLevel)
        .CreateBootstrapLogger();

      // and run
      return await CliBuilder.Create<Program>().Build().InvokeAsync(args);
    }
  }
}