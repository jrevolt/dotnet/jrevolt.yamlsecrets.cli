﻿using System.Threading.Tasks;
using JRevolt.Configuration.CommandLine;

namespace JRevolt.YamlSecrets.Cli
{
  [CliCommand("decrypt,dec")]
  [CliDescription("Decrypt value/file/stdin using provided private key.")]
  internal class DecryptCommand : BaseInOutCommand
  {
    [CliEntrypoint]
    internal async Task Run()
    {
      LogInfo();
      var secrets = CreateSecrets();

      using var reader = CreateReader();
      var content = await reader.ReadToEndAsync();
      reader.Close();

      var decrypted = YamlMode
        ? await new YamlDecryptor(secrets).ProcessYaml(content)
        : secrets.Decrypt(content);
      await using var writer = CreateWriter();
      await writer.WriteAsync(decrypted);
      await writer.FlushAsync();
    }
  }
}