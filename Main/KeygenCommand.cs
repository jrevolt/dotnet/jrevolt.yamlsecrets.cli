﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using JRevolt.Configuration.CommandLine;
using PemUtils;

namespace JRevolt.YamlSecrets.Cli
{
  [CliCommand]
  [CliDescription("Generate private/public key pair")]
  internal class KeygenCommand : BaseCommand
  {
    [CliOption("-i,--private-key-file")]
    [CliDescription("Path to private key file, to be generated.")]
    [Required]
    public string PrivateKeyFile { get; protected set; }

    [CliOption]
    [CliDescription("Path to public key file. Optional, derived from private key file name.")]
    public string? PublicKeyFile { get; internal set; }

    [CliOption]
    public bool Overwrite { get; protected set; }

    [CliEntrypoint]
    internal void Run()
    {
      var fkey = new FileInfo(PrivateKeyFile);
      if (fkey.Exists && !Overwrite) throw new ArgumentException($"File exists: {fkey.FullName}");

      if (PublicKeyFile is null)
      {
        var baseName = Regex.Replace(fkey.Name, $"{fkey.Extension}$", "");
        PublicKeyFile = Path.Join(fkey.DirectoryName, $"{baseName}.pub");
      }

      var fpub = new FileInfo(PublicKeyFile);
      if (fpub.Exists && !Overwrite) throw new ArgumentException($"File exists: {fpub.FullName}");

      Log.Information("Generating private key {0}, exporting public key {1} in folder {2}", fkey.Name, fpub.Name, fkey.Directory);

      var rsa = new RSACryptoServiceProvider();

      string key, pub;
      {
        using var buf = new MemoryStream();
        using var w = new PemWriter(buf, encoding: Encoding.ASCII);
        w.WritePrivateKey(rsa);
        key = Encoding.ASCII.GetString(buf.ToArray());
      }
      {
        using var buf = new MemoryStream();
        using var w = new PemWriter(buf, encoding: Encoding.ASCII);
        w.WritePublicKey(rsa);
        pub = Encoding.ASCII.GetString(buf.ToArray());
      }

      // validate
      var keyOk = new Secrets(key).PrivateKey is not null;
      var pubOk = new Secrets(key).PublicKey is not null;

      if (!(keyOk && pubOk)) throw new ApplicationException("Key pair generated but failed to load.");

      // save
      File.WriteAllText(PrivateKeyFile, key, Encoding.ASCII);
      File.WriteAllText(PublicKeyFile, pub, Encoding.ASCII);
    }
  }
}