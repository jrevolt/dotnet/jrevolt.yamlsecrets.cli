﻿using System.Threading.Tasks;
using JRevolt.Configuration.CommandLine;

namespace JRevolt.YamlSecrets.Cli
{
  [CliCommand("encrypt,enc")]
  [CliDescription("Encrypt value/file/stdin using provided public key.")]
  internal class EncryptCommand : BaseInOutCommand
  {
    [CliEntrypoint]
    public async Task Run()
    {
      LogInfo();
      var secrets = CreateSecrets();

      using var reader = CreateReader();
      var content = await reader.ReadToEndAsync();
      reader.Close();

      var encrypted = YamlMode
        ? await new YamlEncryptor(secrets).ProcessYaml(content)
        : secrets.Encrypt(content);
      await using var writer = CreateWriter();
      await writer.WriteAsync(encrypted);
      await writer.FlushAsync();
    }
  }
}