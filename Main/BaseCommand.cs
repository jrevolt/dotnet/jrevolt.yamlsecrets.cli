﻿using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using JRevolt.Configuration.CommandLine;
using Serilog;

namespace JRevolt.YamlSecrets.Cli
{
  internal abstract class BaseCommand
  {
    protected ILogger Log => Serilog.Log.ForContext(GetType());
  }

  internal abstract class BaseInOutCommand : BaseCommand
  {
    [CliOption("-k")]
    [Required]
    [CliDescription("RSA key, public or private, file or inline, PEM encoded.")]
    public string Key { get; }

    [CliOption("-f")]
    [CliDescription("Input file. Optional, defaults to stdin.")]
    public string Input { get; }

    [CliOption("-v")]
    [CliDescription("Input value as a single parameter. Input (-f) will be ignored.")]
    public string? Value { get; }

    [CliOption("-o")]
    [CliDescription("Output file. Optional, defaults to stdout.")]
    public string Output { get; }

    [CliOption("-y")]
    [CliDescription("Enables YAML mode. Defaults to false (plain text)")]
    public bool YamlMode { get; }

    [CliOption("-i,--in-place")]
    [CliDescription("Edit the specified file directly in-place.")]
    public bool EditInPlace { get; }

    protected bool IsStandardInput() => Input is null or "-" && !IsValueInput();
    protected bool IsFileInput() => !IsStandardInput() && File.Exists(Input);
    protected bool IsValueInput() => Value is not null;

    protected bool IsStandardOutput() => Output is null or "-";

    internal TextReader CreateReader() =>
      Value is not null ? new StringReader(Value) :
      IsStandardInput() ? Console.In :
      IsFileInput() ? new StreamReader(Input!) :
      throw new ArgumentOutOfRangeException();

    internal TextWriter CreateWriter() =>
      IsFileInput() && EditInPlace ? new StreamWriter(Input) :
      Output is null or "-" ? Console.Out : new StreamWriter(Output)
    ;

    internal Secrets CreateSecrets() => new(Key);

    protected void LogInfo()
    {
      Log.Debug("{0}, version: {1}", GetType().Assembly.GetName().Name,
        GetType().Assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion);
      Log.Debug("Reading input from {0}, using key from {1}, writing to {2}",
        IsStandardInput() ? "stdin" : IsFileInput() ? $"file: {Input}" : "provided value",
        File.Exists(Key) ? $"file: {Key}" : "provided value",
        IsStandardOutput() ? "stdout" : $"file: {Output}");
    }
  }

}