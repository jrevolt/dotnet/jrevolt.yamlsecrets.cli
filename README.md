# JRevolt.YamlSecrets.Cli

## overview

This is a simple tool primarily designed to encrypt selected data in YAML
files.

Typical use case:

original:

```yaml
---
user: johndoe
password: !<secret> "topsecret"
```

encrypted:

```yaml
---
user: johndoe
password: !<encrypted> "//+AAB...n4Y"
```

Tool can be used to encrypt/decrypt any UTF-8 encoded text file, not just YAMLs.

:warning: Binary data and/or custom text encodings are out of scope at the moment.

For details about actual encryption process, see
https://gitlab.com/jrevolt/dotnet/jrevolt.yamlsecrets

## usage

```
Usage:
  JRevolt.YamlSecrets.Cli [options] [command]

Options:
  --version       Show version information
  -?, -h, --help  Show help and usage information

Commands:
  dec, decrypt  Decrypt value/file/stdin using provided private key.
  enc, encrypt  Encrypt value/file/stdin using provided public key.
  keygen        Generate private/public key pair
```

## generate key

```
keygen
  Generate private/public key pair

Usage:
  JRevolt.YamlSecrets.Cli [options] keygen

Options:
  -i, --private-key-file <PrivateKeyFile> (REQUIRED)  Path to private key file, to be generated.
  --public-key-file <PublicKeyFile>                   Path to public key file. Optional, derived from private key file name.
  --overwrite
```

```shell
$ yaml-secrets keygen -i secret.key
$ ls -l secret.{key,pub}
```

### encrypt values/files

```
encrypt
  Encrypt value/file/stdin using provided public key.

Usage:
  JRevolt.YamlSecrets.Cli [options] encrypt

Options:
  -k <Key> (REQUIRED)  RSA key, public or private, file or inline, PEM encoded.
  -f <Input>           Input file. Optional, defaults to stdin.
  -v <Value>           Input value as a single parameter. Input (-f) will be ignored.
  -o <Output>          Output file. Optional, defaults to stdout.
  -y                   Enables YAML mode. Defaults to false (plain text)
  -i, --in-place       Edit the specified file directly in-place.
```

```shell
$ yaml-secrets enc -k secret.pub -v topsecret
//+AAIdGE/xO/...AN1c
```

encrypt YAML:

```shell
$ echo '{user: johndoe, pass: !<secret> topsecret}' | yaml-secrets.exe enc -y -k secret.pub
{user: johndoe, pass: !<encrypted> "//+AAB...n4Y"}
```

encrypt YAML file in-place:

```shell
$ echo '{user: johndoe, pass: !<secret> topsecret}' | tee test.yaml
$ yaml-secrets enc -y -k secret.pub -f test.yaml -i
$ cat test.yaml
{user: johndoe, pass: !<encrypted> "//+AAB...n4Y"}
```

## decrypt values/files

```
decrypt
  Decrypt value/file/stdin using provided private key.

Usage:
  JRevolt.YamlSecrets.Cli [options] decrypt

Options:
  -k <Key> (REQUIRED)  RSA key, public or private, file or inline, PEM encoded.
  -f <Input>           Input file. Optional, defaults to stdin.
  -v <Value>           Input value as a single parameter. Input (-f) will be ignored.
  -o <Output>          Output file. Optional, defaults to stdout.
  -y                   Enables YAML mode. Defaults to false (plain text)
  -i, --in-place       Edit the specified file directly in-place.
```

```shell
$ encrypted=$(yaml-secrets enc -k secret.pub -v topsecret)
$ yaml-secrets dec -k secret.key -v "$encrypted"
topsecret
```

decrypt YAML data

```shell
$ encrypted=$(echo '{user: johndoe, pass: !<secret> topsecret}' | yaml-secrets enc -y -k secret.pub)
$ echo "$encrypted"
{user: johndoe, pass: !<encrypted> "//+AAL...osR5"}

$ echo "$encrypted" | yaml-secrets dec -y -k secret.key
{user: johndoe, pass: !<secret> "topsecret"}
```

or, operate on file:

```shell
$ echo "$encrypted" > test.yaml
$ yaml-secrets dec -y -k secret.key -f test.yaml
{user: johndoe, pass: !<secret> "topsecret"}

#or, decrypt file in-place:
$ yaml-secrets dec -y -k secret.key -f test.yaml -i
$ cat test.yaml
```
