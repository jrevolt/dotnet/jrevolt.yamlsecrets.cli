#syntax=docker/dockerfile:1.2
FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /work
ADD . ./
RUN --mount=type=cache,target=/root \
    dotnet publish -f net6.0 -o /dist/

FROM mcr.microsoft.com/dotnet/runtime:6.0-alpine as runtime
ENTRYPOINT [ "yaml-secrets" ]
RUN --mount=from=build,source=/dist,target=/mnt/yaml-secrets \
    mkdir -p /opt &&\
    cp -r /mnt/yaml-secrets/ /opt/ &&\
    ln -s /opt/yaml-secrets/JRevolt.YamlSecrets.Cli /usr/local/bin/yaml-secrets
    
